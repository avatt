#############################################################
#
# vzctl
#
#############################################################
VZCTL_VERSION:=3.0.23
VZCTL_SOURCE:=vzctl-$(VZCTL_VERSION).tar.bz2
VZCTL_SITE:=http://download.openvz.org/utils/vzctl/$(VZCTL_VERSION)/src/
VZCTL_AUTORECONF:=NO
VZCTL_INSTALL_STAGING:=NO
VZCTL_INSTALL_TARGET:=YES

VZCTL_DEPENDENCIES:=uclibc

$(eval $(call AUTOTARGETS,package,vzctl))

