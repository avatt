#############################################################
#
# qemu
#
#############################################################
QEMU_VERSION:=0.10.5
QEMU_SOURCE:=qemu-$(QEMU_VERSION).tar.gz
QEMU_SITE:=http://download.savannah.gnu.org/releases/qemu/
QEMU_AUTORECONF:=NO
QEMU_INSTALL_STAGING:=NO
QEMU_INSTALL_TARGET:=YES

QEMU_DEPENDENCIES:=uclibc

$(eval $(call AUTOTARGETS,package,qemu))

