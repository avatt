#############################################################
#
# vzquota
#
#############################################################
VZQUOTA_VERSION:=3.0.12
VZQUOTA_SOURCE:=vzquota-$(VZQUOTA_VERSION).tar.bz2
VZQUOTA_SITE:=http://download.openvz.org/utils/vzquota/$(VZQUOTA_VERSION)/src/
VZQUOTA_CAT:=$(BZCAT)
VZQUOTA_DIR:=$(BUILD_DIR)/vzquota-$(VZQUOTA_VERSION)
VZQUOTA_BINARY:=vzquota
VZQUOTA_TARGET_BINARY:=usr/bin/vzquota

$(DL_DIR)/$(VZQUOTA_SOURCE):
	 $(call DOWNLOAD,$(VZQUOTA_SITE),$(VZQUOTA_SOURCE))

vzquota-source: $(DL_DIR)/$(VZQUOTA_SOURCE)

$(VZQUOTA_DIR)/.unpacked: $(DL_DIR)/$(VZQUOTA_SOURCE)
	$(VZQUOTA_CAT) $(DL_DIR)/$(VZQUOTA_SOURCE) | tar -C $(BUILD_DIR) $(TAR_OPTIONS) -
	toolchain/patch-kernel.sh $(VZQUOTA_DIR) package/vzquota/ \*.patch
	touch $@

$(TARGET_DIR)/$(VZQUOTA_TARGET_BINARY): $(VZQUOTA_DIR)/.unpacked
	$(MAKE) DESTDIR=$(TARGET_DIR) -C $(VZQUOTA_DIR) install

vzquota: uclibc $(TARGET_DIR)/$(VZQUOTA_TARGET_BINARY)

vzquota-clean:
	$(MAKE) DESTDIR=$(TARGET_DIR) -C $(VZQUOTA_DIR) uninstall
	-$(MAKE) -C $(VZQUOTA_DIR) clean

vzquota-dirclean:
	rm -rf $(VZQUOTA_DIR)

#############################################################
#
# Toplevel Makefile options
#
#############################################################
ifeq ($(BR2_PACKAGE_VZQUOTA),y)
TARGETS+=vzquota
endif
