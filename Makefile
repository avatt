all: buildrom/buildrom-devel

buildrom/buildrom-devel: buildroot buildrom/buildrom-devel/.config
	make -C buildrom/buildrom-devel

buildroot: buildroot/.config
	make -C buildroot

defconfig: buildrom-defconfig buildroot-defconfig

buildrom-defconfig:
	@ cp -f configs/buildrom.config buildrom/buildrom-devel/.config 
buildroot-defconfig:
	@ make -C buildroot defconfig
	@ cp -f configs/buildroot.config buildroot/.config

buildroot-config:
	make -C buildroot menuconfig
buildrom-config:
	make -C buildrom/buildrom-devel menuconfig

test: buildrom/buildrom-devel/deploy/emulation-qemu-x86.rom
	qemu -L qemu-bios -hda qemu-bios/diskimage.qcow2 -net user -net nic,model=pcnet &

clean: buildroot-clean buildrom-clean

distclean:
	make -C buildroot distclean
	make -C buildrom/buildrom-devel distclean

buildroot-clean: rom-purge
	make -C buildroot clean

buildrom-clean: rom-purge
	make -C buildrom/buildrom-devel clean

rom-purge:
	@ rm -f buildrom/buildrom-devel/deploy/buildroot-payload.elf
	@ rm -f buildrom/buildrom-devel/deploy/emulation-qemu-x86.rom
	@ rm -fr buildroot/project_build_i686/avatt/root

.PHONY: all config-buildroot config-buildrom rom-purge buildrom/buildrom-devel \
	buildroot buildrom-defconfig buildroot-defconfig
